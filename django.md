# Django

## Content:

- <a href="#command">Command</a>
- <a href="#models">Модели(models.py)</a>
- <a href="#settings">Настроечные параметры</a>
- <a href="#admin">Сайт администрирования</a>
- <a href="#queryset">Работа с запросами</a>
- <a href="#model_manager">Модельные менеджеры</a>
- <a href="#views">Представлени(views)</a>
- <a href="#urls">Urls</a>
- <a href="#paginator">Пагинатор</a>



<a name="command"></a>

## Command

```bash
# создание проекта
django-admin startproject <name> 

# запуск собственного сервера
python3 manage.py runserver

# создание приложения
python3 manage.py startapp <name> 

# создание таблиц для приложений, перечисленных в параметре INSTALLED_APPS
python3 manage.py migrate 

# использование определенного настроечного файла
python3 manage.py runserver 127.0.0.1:8001 --settings=mysite.settings 

# открытие оболочки python
python3 manage.py shell

# создание миграций
python manage.py makemigrations blog

# применение миграций
python manage.py migrate

# посмотреть sql запрос, который сделала django
python manage.py sqlmigrate blog 0001

# создать суперпользователя
python manage.py createsuperuser


```

<a name="models"></a>

## Модели(models.py)

```py
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

class Post(models.Model):

    class Status(models.TextChoices):
        DRAFT = 'DF', 'Draft'
        PUBLISHED = 'PB', 'Published'

    # VARCHAR
    title = models.CharField(max_length=250) 
    # VARCHAR и короткая метка
    slug = models.SlugField(max_length=250)
    # Text
    body = models.TextField()
    # DATETIME
    publish = models.DateTimeField(default=timezone.now)
    # дата автоматически сохраняется при создании объекта
    created = models.DateTimeField(auto_now_add=True) 
    # дата автоматически сохраняется при сохранении объекта
    updated = models.DateTimeField(auto_now=True)
    # Поле выбора: 
    # Status.choices - [('DF', 'Draft'), ('PB', 'Published')]
    # Status.labels - ['Draft', 'Published']
    # Status.values - ['DF', 'PB']
    # Status.names - ['DRAFT', 'PUBLISHED']
    status = models.CharField(max_length=2,
                              choices=Status.choices,
                              default=Status.DRAFT)
    # Многие-к-одному, при удалении пользователя удалятся и его посты
    # blog_posts - имя обратной связи, от User к Post
    author = models.ForeignKey(User,
                               on_delete=models.CASCADE,
                               related_name='blog_posts')

    class Meta:
        ordering = ['-publish']
        indexes = [
            models.Index(fields=['-publish']),
        ]

    # отображение имени объекта
    def __str__(self):
        return self.title

    # формирует url и используется также в шаблонах в href например
    def get_absolute_url(self):
        return reverse('blog:post_detail',
                       args=[self.id])

```

<a name="settings"></a>

## Настроечные параметры

- DEBUG – это булев параметр, который включает и выключает режим отладки проекта. Если его значение установлено равным True, то Django будет отображать подробные страницы ошибок в случаях, когда приложение выдает неперехваченное исключение. При переходе в производственную среду следует помнить о том, что необходимо устанавливать его значение равным False. Никогда не развертывайте свой сайт в производственной среде с включенной отладкой, поскольку вы предоставите конфиденциальные данные, связанные с проектом.
- ALLOWED_HOSTS не применяется при включенном режиме отладки или при выполнении тестов. При перенесении своего сайта в производственную среду и установке параметра DEBUG равным False в этот настроечный параметр следует добавлять свои домен/хост, чтобы разрешить ему раздавать ваш сайт Django.
- INSTALLED_APPS – это параметр, который придется редактировать во всех проектах. Он сообщает Django о приложениях, которые для этого сайта являются активными. По умолчанию Django вставляет следующие ниже приложения:
– django.contrib.admin: сайт администрирования;
– django.contrib.auth: фреймворк аутентификации;
– django.contrib.contenttypes: фреймворк типов контента;
– django.contrib.sessions: фреймворк сеансов;
– django.contrib.messages: фреймворк сообщений;
– django.contrib.staticfiles: фреймворк управления статическими файлами.
- MIDDLEWARE – подлежащие исполнению промежуточные программные компоненты.
- ROOT_URLCONF указывает модуль Python, в котором определены шаблоны корневых URL-адресов приложения.
- DATABASES – словарь, содержащий настроечные параметры всех баз данных, которые будут использоваться в проекте. Всегда должна существовать база данных, которая будет использоваться по умолчанию. В стандартной конфигурации используется база данных SQLite3, если не указана иная.
- LANGUAGE_CODE определяет заранее заданный языковой код этого сайта Django.
- USE_TZ сообщает Django, что нужно активировать/деактивировать поддержку часовых поясов. Django поставляется вместе с поддержкой дат и времен с учетом часовых поясов. Этот настроечный параметр получает значение True при создании нового проекта с помощью команды управления startproject.


<a name="admin"></a>

## Сайт администрирования

```py
from django.contrib import admin
from .models import Post


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ['title', 'slug', 'author', 'publish', 'status']
    list_filter = ['status', 'created', 'publish', 'author']
    search_fields = ['title', 'body']
    prepopulated_fields = {'slug': ('title',)}
    raw_id_fields = ['author']
    date_hierarchy = 'publish'
    ordering = ['status', 'publish']


```


<a name="queryset"></a>

## Работа с запросами

```py
Post.objects.all() # извлечение всех запросов
Post.objects.filter(publish__year=2022) # фильтрация набора
Post.objects.exclude(title__startswith='Why') # исключение из набора
Post.objects.order_by('title') # сортировка
post.delete() # удаление объекта
```

<a name="model_manager"></a>

## Модельные менеджеры

По умолчанию в каждой модели используется менеджер objects. Этот менеджер извлекает все объекты из базы данных. Однако имеется возможность определять конкретно-прикладные модельные менеджеры.

```py
class PublishedManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset()\
            .filter(status=Post.Status.PUBLISHED)

class Post(models.Model):
    objects = models.Manager() # менеджер, применяемый по умолчанию
    published = PublishedManager() # конкретно-прикладной менеджер
```


<a name="views"></a>

## Представлени(views)

```py

from django.shortcuts import render, get_object_or_404
from .models import Post


# Create your views here.

def post_list(request):
    posts = Post.published.all()
    return render(
        request,
        'blog/post/list.html',
        {'posts': posts}
    )


def post_detail(request, id):
    post = get_object_or_404(
        Post,
        id=id,
        status=Post.Status.PUBLISHED)
    return render(
        request,
        'blog/post/detail.html',
        {'post': post}
    )

```
### На основе классов 

### Views

```py
from django.views.generic import ListView


# Create your views here.

class PostListView(ListView):
    queryset = Post.published.all()
    context_object_name = 'posts'
    paginate_by = 3
    template_name = 'blog/post/list.html'

```

### Url

```py
 path('', views.PostListView.as_view(), name='post_list')
```

### Template

```jinja
{% include 'pagination.html' with page=page_obj %}
```

<a name="urls"></a>

## Urls

### В приложении

```py
from django.urls import path
from . import views

app_name = 'blog'

urlpatterns = [
    path('', views.post_list, name='post_list'),
    path('<int:id>/', views.post_detail, name='post_detail'),
]
```

### В проекте


```py
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('blog/', include('blog.urls', namespace='blog')),
]
```

<a name="template"></a>

## Template

Django обладает мощным языком шаблонов, который позволяет указывать внешний вид отображения данных. Он основан на шаблонных тегах, шаблонных переменных и шаблонных фильтрах:
- шаблонные теги управляют прорисовкой шаблона и выглядят как {% tag %};
- шаблонные переменные заменяются значениями при прорисовке шаблона и выглядят как {{ variable }};
- шаблонные фильтры позволяют видоизменять отображаемые переменные и выглядят как {{ variable|filter }}

![struct](images/template_django.png)


### Базовый

```html
{% load static %}
<!DOCTYPE html>
<html>
    <head>
        <title>{% block title %}{% endblock %}</title>
        <link href="{{ post.get_absolute_url }}" rel="stylesheet">
    </head>
    <body>
        <div id="content">
            {% block content %}
            {% endblock %}
        </div>
        <div id="sidebar">
            <h2>My blog</h2>
            <p>This is my blog.</p>
        </div>
    </body>
</html>
```

### Наследующие 

```py
{% extends "blog/base.html" %}

{% block title %}My Blog{% endblock %}

{% block content %}
    <h1>My Blog</h1>
    {% for post in posts %}
        <h2>
            <a href="{% url 'blog:post_detail' post.id %}">
                {{  post.title }}
            </a>
        </h2>
        <p class="date">
        Published {{ post.publish }} by {{ post.author }}
        </p>
        {{ post.body|truncatewords:30|linebreaks }}
    {% endfor %}
{% endblock %}
```

<a name="paginator"></a>

## Пагинатор

### В views

```py
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


# Create your views here.

def post_list(request):
    post_list = Post.published.all()
    paginator = Paginator(post_list, 3)
    page_number = request.GET.get('page', 1)
    try:
        posts = paginator.page(page_number)
    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)

    return render(
        request,
        'blog/post/list.html',
        {'posts': posts}
    )
```


### Базовый шаблон пагинации(внутри template)

```jinja
<div class="pagination">
<span class="step-links">
{% if page.has_previous %}
    <a href="?page={{ page.previous_page_number }}">Previous</a>
{% endif %}
    <span class="current">
Page {{ page.number }} of {{ page.paginator.num_pages }}.
</span>
    {% if page.has_next %}
        <a href="?page={{ page.next_page_number }}">Next</a>
    {% endif %}
</span>
</div>
```

### Исользование в шаблонах

```jinja
{% include 'pagination.html' with page=posts %}
```