# MongoDB

```
mongodb://localhost:27017
```

![struct_db](images/mongo_collections.png)

## Content:

- <a href="#command">Основные команды</a>
- <a href="#operators">Условные операторы</a>

<a name="command"></a>

## Основные команды

```sql
use <newdb> # переключение на существующую или создание новой БД
show dbs # просмотр баз данных
show collections # просмотр коллекций
db.stats() # Статистика по БД
db.<collection>.stats() # Статистика по коллекции БД
db.createCollection("...") # создание коллекции
db.<collection>.find() # выводит все документы коллекции
db.<collection>.find({"languages.0": "spanish"}) # у которых первый элемент languages это spanish
db.<collection>.find({name: "Tom"}, {age: 0}) # не выводить поле age(проекция)
db.<collection>.find({}, {age: 0}) # без поля age, но все записи 
db.<collection>.find({"company.name": "Microsoft"}) # запрос к вложенным объектам
db.<collection>.find({name:/^B\w+/i}) # использование регулярных выражений 
db.<collection>.findOne() # вывести только один документ

# Курсоры
var cursor = db.users.find()
 
while(cursor.hasNext()){
  obj = cursor.next();
  print(obj["name"]);
}

# пропускает n-ое кол-во документов
db.<collection>.skip() 
db.<collection>.limit() # выводит ограниченное кол-во документов
db.<collection>..sort({name: 1}) # по возрастанию (1) или по убыванию (-1)
# $slice: limit
# $slice: skip, limit
# db.users.find ({name: "Tom"}, {languages: {$slice : [-2, 1]}}); # второй элемент с конца
db.users.createIndexes([{"name" : 1}, {"age": 1}]) # создание индекса
db.users.dropIndex("name_1") # удаление индекса, передается имя индекса
db.users.createIndex({"name" : 1}, {"unique" : true}) # чтобы в индексированных полях были только уникальные значения
db.users.getIndexes() # просмотр индексов
db.users.countDocuments() # количество документов
db.users.find({name: "Tom"}).count() # количество для выборки
db.users.find({name: "Tom"}).skip(2).count(true) # true, чтобы можно было использовать count, так как по умолчанию его нельзя использовать
db.users.distinct("name") # уникальные значения по полю name
# Функции min и max устанавливает для определенного поля минимальное значение для попадания в выборку.
# функцию hint(), в которую передается индекс
db.users.find().min({age:30}).hint({age:1}) 
```


<a name="operators"></a>

## Условные операторы

- $eq (равно)
- $ne (не равно)
- $gt (больше чем)
- $lt (меньше чем)
- $gte (больше или равно)
- $lte (меньше или равно)
- $in определяет массив значений, одно из которых должно иметь поле документа
- $nin определяет массив значений, которые не должно иметь поле документа

Example:
```
db.users.find({age: {$gt : 30, $lt: 50}}) # больше 30 и меньше 50
db.users.find ({age: {$in : [22, 32]}}) # возраст 22 или 32
db.users.find ({age: {$nin : [22, 32]}}) # возраст не 22 и не 32
```