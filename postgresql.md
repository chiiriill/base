# Postgres

```
http://127.0.0.1/pgadmin4/browser/ 
```

## Content:

- <a href="#command">Основные команды</a>


<a name="command"></a>

## Основные команды

```sql
# Создание пользователя
CREATE USER anton WITH PASSWORD 'j8IIj_p0kZ';
# Создание базы данных
CREATE DATABASE skillbox_test_db;
# Выдача привилегий на работу с базой данных
GRANT ALL PRIVILEGES ON DATABASE skillbox_test_db to anton;
```


<a name="links"></a>

## Links

- https://skillbox.ru/media/code/postgresql-vsye-chto-nuzhno-znat-dlya-bystrogo-starta/#stk-4