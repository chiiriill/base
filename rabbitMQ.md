```py
pika==1.3.2
```

# RabbitMQ

RabbitMQ позволяет взаимодействовать различным программам при помощи протокола AMQP(Advanced message queue protocol)

RabbitMQ ‒ это брокер сообщений. Его основная цель ‒ принимать и отдавать сообщения.

### Send.py

```py
import pika
# Подключение к брокеру сообщений
connection = pika.BlockingConnection(pika.ConnectionParameters(
    host='localhost'))
channel = connection.channel()


# Создание очереди
channel.queue_declare(queue='hello', durable=True)
# durable=True обеспечивает сохраность очереди при аварийном отключении

# Отправка сообщения 
channel.basic_publish(exchange='',
                      routing_key='hello',
                      body=b'Hello World!',
                      properties=pika.BasicProperties(
        delivery_mode=pika.spec.PERSISTENT_DELIVERY_MODE # обеспечивает сохраность сообщений при аварийном отключении
    ))
print(" [x] Sent 'Hello World!'")

# Безопасное закрытие с брокером
connection.close()
```

### Receiver.py

```py
import pika

connection = pika.BlockingConnection(pika.ConnectionParameters(
    host='localhost'))
channel = connection.channel()

# Если очередь найдена, то не будет создавать новую
channel.queue_declare(queue='hello', durable=True)
# durable=True обеспечивает сохраность очереди при аварийном отключении

print(' [*] Waiting for messages. To exit press CTRL+C')

def callback(ch, method, properties, body):
    print(" [x] Received %r" % (body,))
    ch.basic_ack(delivery_tag=method.delivery_tag) # отправляет подтверждение, что сообщение обработано

channel.basic_qos(prefetch_count=1) # отправлять по одному сообщению за раз
channel.basic_consume(on_message_callback=callback,
                      queue='hello')

# Запуск бесконечного ожидания сообщения и вызова функции callback
channel.start_consuming()
```

# Template "publish/subscribe"

There are a few exchange types available: `direct`, `topic`, `headers` and `fanout`


### Receiver.py

```py
import pika

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.exchange_declare(exchange='logs', exchange_type='fanout')

result = channel.queue_declare(queue='', exclusive=True)
queue_name = result.method.queue

channel.queue_bind(exchange='logs', queue=queue_name)

print(' [*] Waiting for logs. To exit press CTRL+C')


def callback(ch, method, properties, body):
    print(" [x] %r" % body)


channel.basic_consume(
    queue=queue_name, on_message_callback=callback, auto_ack=True)

channel.start_consuming()
```

### Send.py

```py
import pika
import sys

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.exchange_declare(exchange='logs', exchange_type='fanout')

message = ' '.join(sys.argv[1:]) or "info: Hello World!"
channel.basic_publish(exchange='logs', routing_key='', body=message)
print(" [x] Sent %r" % message)
connection.close()
```

# Маршрутизация

Мы использовали `fanout` обмен, который не дает нам особой гибкости — он способен только на бездумное вещание.

Вместо этого мы будем использовать `direct` обмен. Алгоритм маршрутизации при прямом обмене прост — сообщение попадает в очереди, `binding key` которых точно соответствует `routing key` сообщения.

### Receiver:

```py
import pika
import sys

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.exchange_declare(exchange='direct_logs', exchange_type='direct')

result = channel.queue_declare(queue='', exclusive=True)
queue_name = result.method.queue

severities = sys.argv[1:]
if not severities:
    sys.stderr.write("Usage: %s [info] [warning] [error]\n" % sys.argv[0])
    sys.exit(1)

for severity in severities:
    channel.queue_bind(
        exchange='direct_logs', queue=queue_name, routing_key=severity)

print(' [*] Waiting for logs. To exit press CTRL+C')


def callback(ch, method, properties, body):
    print(" [x] %r:%r" % (method.routing_key, body))


channel.basic_consume(
    queue=queue_name, on_message_callback=callback, auto_ack=True)

channel.start_consuming()
```

### Send:

```py 
import pika
import sys

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.exchange_declare(exchange='direct_logs', exchange_type='direct')

severity = sys.argv[1] if len(sys.argv) > 1 else 'info'
message = ' '.join(sys.argv[2:]) or 'Hello World!'
channel.basic_publish(
    exchange='direct_logs', routing_key=severity, body=message)
print(" [x] Sent %r:%r" % (severity, message))
connection.close()
```

# Topics

Сообщения, отправляемые в тему обмена, не могут иметь произвольный routing_key — это должен быть список слов, разделенных точками. Слова могут быть любыми, но обычно они определяют некоторые особенности, связанные с сообщением. Несколько допустимых примеров ключей маршрутизации: « stock.usd.nyse », « nyse.vmw », « quick.orange.rabbit ». В ключе маршрутизации может быть сколько угодно слов, но не более 255 байт.

Ключ привязки также должен быть в той же форме. Логика обмена темами аналогична прямому — сообщение, отправленное с определенным ключом маршрутизации, будет доставлено во все очереди, связанные с соответствующим ключом привязки. Однако есть два важных особых случая привязки ключей:

- \* (звездочка) может заменить ровно одно слово.
- \# (решетка) может заменить ноль или более слов.


