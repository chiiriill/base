# Content:

- <a href="#build">Build</a>
- <a href="#cmake">CMake</a>
- <a href="#vector">Vector</a>
- <a href="#unordered_map">Unordered map</a>
- <a href="#map">Map</a>
- <a href="#set">Set</a>
- <a href="#tuple">Tuple</a>
- <a href="#pair">Pair</a>
- <a href="#deque">Deque</a>
- <a href="#queue">Queue</a>
- <a href="#list">List</a>
- <a href="#array">Array</a>
- <a href="#stack">Stack</a>
- <a href="#string_view">String view</a>
- <a href="#algorithm">Algorithm</a>
- <a href="#iterators">Iterator</a>
- <a href="#time_work">Time work code</a>
- <a href="#lambda">Лямбда-выражения</a>
- <a href="#operator_tern">Тернарный оператор</a>
- <a href="#struct_class">Struct and class</a>
- <a href="#explicit">Explicit</a>
- <a href="#type">Work with type data</a>
- <a href="#templates">Templates</a>
- <a href="#requires">Requires</a>
- <a href="#concepts">Concepts</a>
- <a href="#exceptions">Exceptions</a> 
- <a href="#unit_testing">Unit testing</a> 
- <a href="#documentation">Documentation</a> 
- <a href="#macros">Макросы</a> 
- <a href="#optimization">Оптимизация</a> 
- <a href="#regex">Регулярные выражения</a> 
- <a href="#stream">Stream(потоки)</a> 
- <a href="#algorithm_hard">Сложность алгоритмов</a> 
- <a href="#ranges">Ranges</a> 
- <a href="#threads">Многопоточность(threads)</a> 
- <a href="#constexpr">Константные вычисления</a> 
- <a href="#corutine">Корутины</a> 
- <a href="#new_delete">New && delete</a> 
- <a href="#model_storage">Модель памяти</a> 
- <a href="#gtest">Google test</a> 
- <a href="#move">Move-семантика</a> 
- <a href="#user_literal">Пользовательские литералы</a> 
- <a href="#func_str">Функции для работы со строками</a>

<a name="build"></a>

# Build

```bash
g++ -E <filename> // выполняет препроцессинг и компиляцию файла <filename>
nm <filename> // выводит таблицу имени для программы
T - значит что они расположены в разделе кода программы 
strip <filename> // удалить таблицу имен для файла
nm -C <filename> // вывод таблицы имен с разобранами именами
```

![build_cpp](images/build_cpp.png)

<a name="cmake"></a>

# CMake

```c++
// Минимальная поддерживаемая версия CMake
cmake_minimum_required(VERSION 3.16)

// Имя проекта
project(Example_Hello)

// Создание исполняемого файла и указание требуемых для него зависимостей
add_executable(Example_Hello ../src/Main.cpp)
```

### Создание статической библиотеки

![static_lib](images/static_lib.png)

```c++
// Файл CMakeLists.txt каталога ExampleStatic:

cmake_minimum_required(VERSION 3.16)

project(Example_Static)

// В переменную BINARY_DIR записываем
// полный путь к каталогу построения проекта верхнего уровня
set(BINARY_DIR "${CMAKE_BINARY_DIR}")

// Задание каталога, в который помещается исполняемый файл
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${BINARY_DIR}/bin")
// Задание каталога, в который помещается статическая библиотека 
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${BINARY_DIR}/lib")

// Добавление к построению подпроектов, расположенных в заданных подкаталогах
add_subdirectory(lib)
add_subdirectory(program)

// Файл CMakeLists.txt в lib:

// Создание статической библиотеки с именем mylib
add_library(
            mylib STATIC 
            class_one.cpp 
            class_one.hpp)

// Файл CMakeLists.txt в program

// Создание исполняемого файла
add_executable(Example_Static Main.cpp)

// Добавление каталога lib к списку каталогов, в которых компилятор 
// ищет заголовочные файлы, подключаемые директивами #include
include_directories(../lib)

// Определение подключаемой библиотеки mylib к заданной цели -
// исполняемому файлу Example_Static, передаваемой компоновщику
target_link_libraries(Example_Static mylib)
```

### Создание динамической библиотеки

![static_lib](images/static_lib.png)

```c++
// Файл CMakeLists.txt каталога ExampleStatic:

cmake_minimum_required(VERSION 3.16)

# Название проекта
project(Example_Shared)

# В переменную BINARY_DIR записываем 
#полный путь к каталогу построения проекта верхнего уровня
set(BINARY_DIR "${CMAKE_BINARY_DIR}")

# Задание каталога, в который помещается исполняемый файл
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${BINARY_DIR}/bin")
if(WIN32)
# Задание каталога, в который помещается динамическая библиотека для Windows
# Динамическая библиотека должна находиться в том же каталоге, что и исполняемый файл
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${BINARY_DIR}/bin")
else()
# Задание каталога, в который помещается динамическая библиотека для ОС Linux
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${BINARY_DIR}/lib")
endif()
# Задание каталога, в который помещается библиотека импорта (для Windows)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${BINARY_DIR}/lib")

# Добавление к построению подпроектов, расположенных в заданных подкаталогах
add_subdirectory(lib)
add_subdirectory(program)

// Файл CMakeLists.txt в lib:

// Создание статической библиотеки с именем mylib
add_library(
            mylib SHARED 
            class_one.cpp 
            class_one.hpp)

// Файл CMakeLists.txt в program

// Создание исполняемого файла
add_executable(Example_Static Main.cpp)

// Добавление каталога lib к списку каталогов, в которых компилятор 
// ищет заголовочные файлы, подключаемые директивами #include
include_directories(../lib)

// Определение подключаемой библиотеки mylib к заданной цели -
// исполняемому файлу Example_Static, передаваемой компоновщику
target_link_libraries(Example_Static mylib)
```

### Подключение GTest

```c++
// Файл CMakeLists.txt каталога ExampleGtest:

cmake_minimum_required(VERSION 3.16)

project(Example_Gtest)

// В переменную BINARY_DIR записываем полный путь
// к каталогу построения проекта верхнего уровня
set(BINARY_DIR "${CMAKE_BINARY_DIR}")

// Задание каталога, в который помещается исполняемый файл
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${BINARY_DIR}/bin")
// Задание каталога, в который помещается динамическая библиотека
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${BINARY_DIR}/bin")
// Задание каталога, в который помещается статическая библиотека 
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${BINARY_DIR}/lib")

// Добавление к построению подпроектов, расположенных в заданных подкаталогах
add_subdirectory(src)
add_subdirectory(tst)
add_subdirectory(googletest)

// Файл CMakeLists.txt в src:

// в операционной системе Windows
 set (CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS ON )
// Создание динамической библиотеки с именем Rectan_lib
add_library(
            Rectan_lib SHARED
            Rectan.cpp 
            Rectan.hpp)

// Файл CMakeLists.txt в test:

// Создание исполняемого файла
add_executable(Example_Gtest Main.cpp)

// Добавление каталогов к списку каталогов, в которых компилятор 
// ищет заголовочные файлы, подключаемые директивами #include
include_directories(../googletest)
include_directories(../src)
// Определение подключаемых библиотек Rectan_lib и gtest к заданной цели - исполняемому 
// файлу Example_Gtest, передаваемых компоновщику
target_link_libraries(Example_Gtest Rectan_lib gtest)

```

<a name="vector"></a>

# Vector

Тип vector представляет собой набор элементов одного типа.

![strunct_vector](images/struct_vector.png)

```c++

#include <vector>

vector.resize(31) // меняет длину и сохраняет старые значения
vector.assign(31, false) // меняет длину и инициализирует его false
vector.clear() // очистка вектора
langs.insert (begin(langs), "C++") // вставка перед итератором элемента C++
langs.erase(it, end(langs)); // удалили все с C++ и до конца
v.insert (it, range_begin, range_end ) // вставит диапазон [range_begin, range_end) в позицию it
v.insert (it, {1 , 2, 3}) // вставляет 1, 2, 3 в позицию it
v.insert (it, range_begin, range_end ) // вставит диапазон [range_begin, range_end) в позицию it

v.data() // указатель на первый элемент
v.shrink_to_fit() // очистка памяти от незаполненных элементов
v.reserve(size) // изменение длины на size
```
Также в векторе происходит инвалидация ссылок!

<a name="unordered_map"></a>

# Unordered map and set

Хэш-таблица. Все операции в среднем за константное время

Forward iterator(последовательные итераторы) - нельзя вызывать декремент

После вставки элементов может произойти инвалидация итераторов

Для вставки своих типов данных должен быть перегружен оператор равенства и хэширования:

```c++
struct PlateHash {
    size_t operator()(const Plate& pl) const {
        return pl.Numbers;
    }
};

unordered_set<Plate, PlateHash> pl;

// Для базовых типов можно использовать встроенные алгоритмы

double num = 10.56;
hash<double> h_i;
cout << h_i(num);

```

```c++
#include <unordered_map>
```

<a name="map"></a>

# Map

Этот пример демонстрирует одно из важных свойств словаря: элементы в нем хранятся отсортированными по ключам.

Дело в том, что как только происходит обращение к конкретному элементу словаря с помощью квадратных скобок, компилятор уже создает пару для этого ключа со значением по умолчанию (для целого числа значение по умолчанию — 0).

Красно-черное дерево(сбалансорованое бинарное дерево поиска)

Все опреции за логарифмическую сложность

Итераторы - двунаправленные

Нужно пользоваться строенными методами: find, lower_bound, upper_bound

Чтобы помещать свои структуры у них должен быть перегружен оператор сравнения

### ! Если не важна сортировка, то лучше использовать unordered_map

```c++

#include <map>

map.erase(key) // удаление по ключу


```

<a name="set"></a>

# Set

Красно-черное дерево(сбалансорованое бинарное дерево поиска)

Все опреции за логарифмическую сложность

Итераторы - двунаправленные

Нужно пользоваться строенными методами: find, lower_bound, upper_bound

Чтобы помещать свои структуры у них должен быть перегружен оператор сравнения

```c++

#include <set>

set.insert("Stroustrup") // вставка значения
set.erase("Anton") // удаление по ключу

// extract - извлекает узел из дерева, так как нельзя изменить значение дерева
auto it = tree.begin();
auto node = tree.extract(it);
string& str = node.value();

set1.merge(set2) // объединяет два дерева
```

Создание множества по вектору 

```c++
vector<string> v = {"a", "b", "a"};
set<string> s(begin(v), end(v));
```

<a name="tuple"></a>

# Tuple

Tuple – это кортеж, т. е. структура из ссылок на нужные нам данные (возможно,разнотипные).

```c++
#include <tuple>

// Сравнение дву объектов из разных типов данных
auto lhs_key = tie(lhs.year , lhs.month , lhs.day); // сохраним левую дату
auto rhs_key = tie(rhs.year , rhs.month , rhs.day); // и правую
return lhs_key < rhs_key

tuple <int , string , bool > t(7, "C++", true); // просто создаем кортеж
auto t = make_tuple (7, "C++", true) // создание кортежа
cout << get<1>(t) // обращение по индексу
```

## Замечание: в C++ 17 разрешается не указывать шаблонные параметры tuple в < ... >, т. е. кортеж можно создавать так:
```c++
tuple t(7, "C++", true);
```

<a name="pair"></a>

# Pair

Пара – это частный случай кортежа, но отличие пары в том, что её поля называются first и second и к ним можно обращаться напрямую

```c++
#include <utility>

cout << p.first << " " << p. second << endl; // обращение к элементам 
```

Exemple using:
```c++
Cities cities ;
bool success ;
string Inessage ; // свяжем кортежем ссылок

tie(success , message ) = cities . FindCountry (" Volgograd ");
// or
// auto [success , message ] = cities . FindCountry (" Volgograd ");

cout success << " " << message << endl; // вывели результат
```



<a name="deque"></a>

# Deque (double-ended queue)

Это двусторонняя очередь;

![deque_strunct](images/deque_strunct.png)

Преимущества:
- быстрая вставка в начало;
- при вставке в начало или конец данные не перемещаются, ссылки не инвалидируются;

Недостатки:
- получить элемент по индексу сложнее;
- тяжелее итерироваться: нужно перепрыгивать между чанками;
```c++

#include <deque>

d. push_back (x) // добавление в конец
d. pop_back (x) // удаление из конца
d. push_front (x) // добавление в начало
d. pop_front (x) // удаление из начала
d[i] // обращение к элементу по индексу
```

<a name="queue"></a>

# Queue

Если нужна только очередь, используйте queue;
Основана на деке, но работает немного быстрее;

```c++

#include <queue>

q.push(x), q.pop(x) // вставляем в начало и удаляем из конца
q.front () , q.back () // ссылки на первый и последний элементы очереди
q.size (), q. empty () // размер и проверка на пустоту
```

<a name="list"></a>

# List

![struct_list](images/struct_list.png)

![list_plus](images/list_plus.png)

![list_methods](images/list_methods.png)

![list_it](images/list_it.png)

<a name="array"></a>

# Array

```c++
#include <array>

std::array<int, 5> numbers; // пустой
std::array<int, 5> numbers{}; // заполненный 0
std::array<int, 5> numbers{1, 2, 3}; // 1, 2, 3, 0, 0
std::array number{1, 2, 3}; // С с++ 17 можно не указывать 
```

Основные функции: 

- size(): возвращает размер контейнера
- at(index): возвращает элемент по индексу index
- front(): возвращает первый элемент
- back(): возвращает последний элемент
- fill(n): присваивает всем элементам контейнера значение n

<a name="stack"></a>

# Stack


```c++
#include <stack>

• Как вектор, но умеет меньше:
st.push(x), st.pop(x) // вставляем в конец и удаляем из конца
st.front() , st.back() // ссылки на первый элемент
st.size() , st.empty() // размер и проверка на пустоту
```


<a name="string_view"></a>

# String view

Для этого в стандарте С++17 доступен класс string_view – это ссылка на где-то хранящийся диапазон символов.

```c++
const size_t pos_end = str.npos; // последняя позиция строки
size_t space = str.find(' ', pos); // найти элемент по позиции
str.substr(0, space) // взять подстроку
str.remove_prefix(space + 1); // переместить указатель начала строки на space + 1
```


<a name="algorithm"></a>

# Algorithm

```c++

#include <algorithm>

sort(begin(v), end(v)); // сортировка

count(begin(v), end(v), 2); //количество вхождений

count_if(begin(v), end(v), Gt2); // количество на основе выполнения условий

find_if(begin(v), end(v), Functor); // Находит элемент по условию в функторе

remove_if (begin ( langs ), end( langs ), Functor); // всё что должно остаться теперь вернул в начало и вернул новый конец с помощью erase можно теперь создать итоговый массив

auto it = min_element ( begin ( langs ), end( langs )); // кладём в итератор мин. элемент
cout << ∗it << endl; // min_element может вернуть end(langs), только если он пуст
// C

auto it = max_element ( begin ( langs ), end( langs ));
cout << ∗it << endl;
// Python
// максимальный элемент

auto p =
minmax_element ( begin ( langs ), end( langs )); // пара - min и max в контейнере
cout << ∗p.first << ' ' << ∗p. second << endl;
// C Python

unique – выкидывает в конец идущие подряд дублирования элементов
all_off – проверка условия для всех элементов

auto it = partition ( begin ( langs ), end( langs ), []( const string & lang) {
return lang [0] == 'C'; // делим по принципу "начинается или не начинается на C"
});


// copy_if(Копирование)

vector <string > langs = {" Python ", "C++", "C", "Java", "C#"};
vector <string > c_langs ( langs .size ()); // вектор, куда мы копируем, должен быть //
объявлен
auto it = copy_if ( begin ( langs ), end( langs ), begin ( c_langs ),
[]( const string & lang) {
return lang [0] == 'C';
});
PrintRange (begin ( c_langs ), end( c_langs ));
// C++, C, C#, , ,
Заметим, что размер остался равным 5.

auto it = set_intersection ( begin (a), end(a), begin (b), end(b), begin (v)); // пересечение


vector <string > langs = {" Python ", "C++", "C", "Java", "C#"};
vector <string > c_langs ; // о размере уже не беспокоимся
copy_if (begin ( langs ), end( langs ),
back_inserter ( c_langs ), // специальный итератор, вставляющий в конец
[]( const string & lang) { // привычная лямбда-функция
return lang [0] == 'C'; // снова выделяем только начинающиеся на C
});

cout << it − begin ( langs ) << endl; // таким образом получаем номер элемента в векторе


auto it = s.find (6);
PrintRange (next(it), end(s));
PrintRange (prev(it), end(s));
// 8, 9,
Точно так же работает функция prev, только вычитает 1.

• Проверка на существование:
binary_search ( begin (v), end(v), x)

• Первый больший или равный данному:
lower_bound ( begin (v), end(v), x)

• Первый элемент, больший данного:
upper_bound ( begin (v), end(v), x)

• Диапазон элементов, равных данному (аналог minmax):
equal_range ( begin (v), end(v), x) ==
make_pair ( lower_bound (...) , upper_bound (...) )
 ```


<a name="iterators"></a>

#  Iterator


Чтобы по объекту класса можно было проитерироваться с помощью цикла for, он должен иметь методы begin() и end(). Методы begin() и end() должны возвращать итераторы.

```c++

rbegin (langs ), rend( langs ) // обратный итератор 

```



<a name="time_work"></a>

# Time work code 

### Console command:

```sh
time ./filename
```

### C++ code:

```c++
#include <chrono>
#include <iostream>
#include <vector>
#include <string>

using namespace std;
using namespace std::chrono;


auto start = steady_clock::now();
vector<Person> moscow_population = GetMoscowPopulation();
auto finish = steady_clock::now();
cout << "GetMoscowPopulation "
<< duration_cast<milliseconds>(finish - start).count()
<< " ms" << endl;
```

Переменная хранящая время:
```c++ 
steady_clock::time_point start ;  
```


<a name="lambda"></a>

# Лямбда-выражения

```c++
cout << count_if(begin(v), end(v), [thr](int x) {
if (x > thr) {
return true;
}
return false;
});
```

<a name="operator_tern"></a>

# Тернарный оператор 

```c++
string parity = (x % 2 == 0) ? "even": "odd";
```

<a name="struct_class"></a>

# Struct and class

## Отличия методов от функций:
- Методы вызваются в контексте конкретного объекта.
- Методы имеют доступ к приватным полям (и приватным методам) объекта. К ним можно обращаться просто по названию поля.

Структура (struct) — набор публичных полей. Используется, если не
нужно контролировать консистентность. Типичный пример струк-
туры:

Класс (class) скрывает данные и предоставляет определенный интер-
фейс доступа к ним. Используется, если поля связаны друг с другом
и эту связь нужно контролировать. Пример класса — класс Route,
описанный выше.

Константный метод(не меняет значение полей класса):

```c++
string GetSource() const {
return source;
}

// списки инициализации инициализируются в порядке, в котором указаны в классе(сверху вниз)
Log( string name) : n(name) {
cout << "+ " << n << endl; // пишет + и свой id, когда создаётся
};
```
- virtual добавляется к методам в базовом классе. Позволяет вызывать методы производных классов через ссылку на базовый класс;

- override добавляется к методам в производных классах (классах-потомках). Требует объявления метода в базовом классе с такой же сигнатурой.

### Конструкторы и деструкторы не наследуются, однако можно это сделать

struct MyStr : public string {
    using string::string; // подключение конструктора string
};

<a name="explicit"></a>

# Explicit

Ключевое слово explicit не позволяет вызывать конструктор неявно.

```c++
struct Day {
    int value;
    explicit Day(int new_value) {
        value = new_value;}
};

struct Month {
    int value;
    explicit Month(int new_value) {
        value = new_value;
    }
};

struct Year {
    int value;
    explicit Year(int new_value) {
        value = new_value;
    }
};
```

<a name="files"></a>

# Work with files

Существуют следующие базовые классы:
- istream поток ввода (cin)
- ostream поток вывода (cout)
- iostream поток ввода/вывода

Все остальные классы, о которых пойдет речь далее, от них наследуются.
Классы, которые работают с файловыми потоками:
- ifstream для чтения (наследник istream)
- ofstream для записи (наследник ostream)
- fstream для чтения и записи (наследник iostream)

```c++
getline(input, year, '-'); // считывание до разделителя '-'
input.ignore(1); // пропуск в потоке одного символа

ofstream output(path, ios::app); // открытие файла в режиме дозаписи
stream.peek() // возвращает следующий символ из потока

```

В языке C++ есть файловые манипуляторы, которые работают с потоком и изменяют его поведение. Для того, чтобы с ними работать, нужно подключить библиотеку iomanip.
 - fixed Указывает, что числа далее нужно выводить на экран с фиксиро-
ванной точностью.

```c++
cout << fixed;
```

- setprecision Задает количество знаков после запятой.
```c++
cout << fixed << setprecision(2);
```
- setw (set width) Указывает ширину поля, которое резервируется для вы-
вода переменной.
```c++
cout << fixed << setprecision(2);
cout << setw(10);
```
Этот манипулятор нужно использовать каждый раз при выводе зна-
чения, так как он сбрасывается после вывода следующего значения:
- setfill Указывает, каким символом заполнять расстояние между колонками.
```c++
cout << setfill('.');
```
- left Выравнивание по левому краю поля.
```c++
cout << left;
```

- boolalpha Выводит логический символ строкой(true | false), а не в числовом
```c++
std::cout << boolalpha << true; // cout: true
std::cout  << true; // cout: 1
```

<a name="type"></a>

# Work with type data

Размер типа:

```c++
sizeof (int16_t);
```

Ограничение типов:

```c++
#include <limits>

cout << numeric_limits<unsigned int>::min() << " " << numeric_limits<unsigned int>::max() << endl;
```

Правила выведения общего типа:
1. Перед сравнениями и арифметическими операциями числа приводятся к общему типу;
2. Все типы размера меньше int приводятся к int;
3. Из двух типов выбирается больший по размеру;
4. Если размер одинаковый, выбирается беззнаковый.

Приведение типов:

```c++
static_cast<int>(t.size()) // преобразование к типу int
```

<a name="templates"></a>

# Templates

1. Шаблонные функции объявляются так: template <typename T> T Foo(T var) { ... };
2. Вместо слова typename можно использовать слово class, т. к. в данном контексте они
эквивалентны;
3. Шаблонный тип может автоматом выводиться из контекста вызова функции;
4. После объявления ипользуется, как и любой другой тип;
5. Выведение шаблонного типа может происходить либо автоматически, на основе аргумен-
тов, либо с помощью явного указания в угловых скобках (std::max<double>(2, 3.5));
6. Цель шаблонных функций: сделать код короче (избавившись от дублирования) и универ-
сальнее.

```c++

template<typename U, typename T> // шаблон класса
struct Pair{
    U first;
    T second;
};
// инстанцирование - создание конкретного типа из шаблона(происходит на этапе компиляции) 
Pair<int, string> a; // создание класса


// Если в классе есть конструктор, позволяющий определить тип шаблона, компилятор выводит его сам.
template <typename T> struct Widget {
Widget (T value );
};
Widget w_int (5);

// не указание возвращаемого значения
template <typename X, typename Y>
auto add(X x, Y y) -> decltype(x + y)
{
    return x + y;
}
```

### Variadic-шаблоны(с переменным количеством параметров)

```c++

// Вывод всех аргументов
template<typename... Args>
void PrintAll(Args... args) {
    (std::cout << ... << args);
}

template<typename... Args>
void PrintAll2(Args... args) {
    ((std::cout << ' ' << args), ...);
}
// Вызов метода для всех аргументов 
template<typename... Args>
void CallF(Args... args) {
    (args.F(), ...);
}
// Вызов статического метода 
template<typename... Args>
void CallF() {
    (Args::F(), ...);
}
```

<a name="requires"></a>

# Requires 

## Syntax:

```c++
template <параметры> requires ограничения
содержимое шаблона;
```

- Из заголовка шаблона сразу видно, какие аргументы шаблона разрешены, а какие нет.

- Шаблон создается только в том случае, если аргументы шаблона удовлетворяют всем ограничениям.

- любое нарушение ограничений шаблона приводит к сообщению об ошибке, которое гораздо ближе к первопричине проблемы, а именно к попытке использовать шаблон с неверными аргументами.

```c++
std::is_same<T, double>::value // value равно true если T типа double

```

## Example:

```c++
#include <iostream>
 
template <typename T> requires std::is_same<T, int>::value || std::is_same<T, double>::value
T sum(T a, T b){ return a + b;}
 
int main()
{
    std::cout << sum(3, 4) << std::endl;
    std::cout << sum(12.5, 4.3) << std::endl;
    //std::cout << sum(5l, 7l) << std::endl;  // Error
}
```

<a name="concepts"></a>

# Concepts 

Концепт - ограничение на шаблонный параметр (фильтр).
Концепты - это развитие конструкции std::enable_if и принципа SFINAE.

## Syntax:

```c++
template <параметры>
concept имя_концепта = ограничения;

Example:
template<typename T>
concept Numeric = std::is_integral_v<T> || std::is_floating_point_v<T>;
```

## Using:

```c++
template <typename T>
concept size = sizeof(T) <= sizeof(int);
 
template <typename T> requires size<T> // в качестве ограничения применяется size
T sum(T a, T b){ return a + b;}
 
template <size T> // тоже самое, но сокращенный синтаксис
T sum(T a, T b){ return a + b;}

int main()
{
    std::cout << sum(10, 3) << std::endl;       // 13
    //std::cout << sum(10.6, 3.7) << std::endl; // ! Ошибка
}

// тип T должен поддерживать операцию сложению, при этом ее результат должен представлять тип, который может быть преобразован в тип T (либо сам тип T).
template <typename T>
concept sum_types = requires (T x) { {x + x} -> std::convertible_to<T>; };

// Проверка можно ли выполнить метод F к объекту
template<typename T>
concept HasMethodF = requires (T t) { t.F(); }; 

// Принимает только объекты-диапазоны
template <rng::input_range Range>
void Print(const Range& range) {}
// OR
void Print(const rng::input_range auto& range)

```

## Встроенные концепты:

```c++
#include <concepts>

std::is_same<T, double>::value // value равно true если T типа double
std::convertible_to<K, T> // можно ли преобразоват значение K в значение T
(std::integral<T>) // целое число 
(std::floating_point<T>) //  либо число с плавающей точкой 
```


<a name="exceptions"></a>

# Exceptions 

Proccesing exception:

```c++ 
try {
/* ...код, который потенциально
может дать исключение... */
} catch (exception& ex) {
    ex.what(); // текст исключение(если есть)
/* Обработчик исключения. */
}
```

<a name="unit_testing"></a>

# Unit testing

```c++
#include <cassert>

assert(8 == 9); //faild
assert(9 == 9); //true
```

<a name="documentation"></a>

# Documentation

- Input: итераторы, из которых можно читать (итераторы любых контейнеров). Но не под-
ходят inserter и back_inserter;
- Forward, Bidir: обычные итераторы, из которых можно читать (кроме set и map, если по
forward итератору что-то меняется);
- Random: итераторы, к которым можно прибавлять число или вычитать друг из друга (толь-
ко итераторы векторов и строк);
- Output: итераторы, в которые можно писать (итераторы векторов и строк, inserter и
back_inserter).

Сложность алгоритма:

![hard_algorithm](images/reference_hard_alg.png)

<a name="macros"></a>

# Макросы

```c++
#define MY_MAIN main()
#define FINISH return 0

MY_MAIN {
    FINISH;
}

Оператор # вставляет в код строковое представление параметра макроса;
# define AS_KV (x) #x << " = " << x
cerr << AS_KV(x) << endl

__FILE__ // путь до файла (абсолютный)
__LINE__ // номер строки где вызван
__FILE_NAME__ // имя файла
__TIME__ // время
__DATE__ // дату
```

<a name="optimization"></a>

# Оптимизация


> ## Код надо стараться делать правильным, простым, а только потом быстрым!

## Принципы оптимизации:

1. Избегайте преждевременной оптимизации:
    - 80% времени работы программы тратится на исполнение 20% кода
    - усложнение кода
    - замедление разработки
    - усложнение поддержки кода
2. Замеряйте время выполнение кода
    - с помощью замеров надо находить узкие места

На практике для нахождения медленно работающего места в программе используются специ-
альные инструменты, которые называются профайлерами. Некоторые из них:
- gperftools (Linux, Mac OS);
- perf (Linux);
- VTune (Windows, Linux).



<a name="regex"></a>

# Регулярные выражения


```c++


```



<a name="stream"></a>

# Stream

- Файловый поток ofstream не сразу пишет выводимые в него данные в файл, а накапливает их в промежуточном буфере и сбрасывает его в файл, когда он наполнился. 
- Поток вывода cout и cerr ведет себя точно так же. 
- Манипулятор endl не только выводит перевод строки, но и сбрасывает буфер потока в файл.

Таким образом, использование endl может приводить к снижению скорости вывода в файл или
cout.

- Этот пример нам показывает, что по умолчанию поток cout связан с потоком cin. То есть перед каждой операцией ввода буфер cout и cerr сбрасываются

Поэтому по времени они работают примерно одинаково: 

```c++
{
LOG_DURATION ("endl");
for (int i = 0; i < 100000; ++i) {
int x;
cin >> x;
cout << x << endl;
}
}
{
LOG_DURATION (" '\n'");
for (int i = 0; i < 100000; ++i) {
int x;
cin >> x;
cout << x << endl;
}
}
```

Связывание потоков cin и cout:

```c++
cin.tie(nullptr)
```

Ещё больше ускорить работу программ можно, если прописать строку:

Обязательно надо вызывать до первой операции ввода/вывода

```c++
ios_base::sync_with_stdio(false);
```

<a name="algorithm_hard"></a>

# Сложность алгоритмов

Обсудим сложность алгоритмов.
- Константная (1): арифметические операции, обращение que элементу вектора;
- Логарифмическая (log N ): двоичный поиск (lower_bound и пр.), поиск в set или map;
- Линейная (N ): цикл for (с лёгкими итерациями), алгоритмы find_if, min_element и пр.;
- N log N : алгоритм sort (с лёгкими сравнениями).

Часто используют O-символику. Например, O(log N ) означает, что алгоритм выполняет не больше, чем log N операций с точностью до константы. «O ольшое» означает оценку сверху. В принципе она может не достигаться вообще никогда.

Сложность алгоритма можно посмотреть в документации

![hard_algorithm](images/reference_hard_alg.png)

1. Скорость алгоритмов стоит сравнивать по следующему несложному правилу:
- 1 < log N < N < N log N < N 2 < . . .
2. При сложении большее поглощает меньшее:
- O(N ) + O(log N ) = O(N )
3. O(5N) и O(8N) надо сравнивать измерениями.



<a name="ranges"></a>

# Ranges



```c++
#include <ranges>

namespace rng = std::ranges;
namespace view = rng::views;

auto numbers_out = numbers_in | view::filter(even) |  // фильтрует
    view::transform(half);  // применяет к каждому элементу изменения

rng::sort(numbers_in); // сортирует но принимает последовательность, надо также подключить <algorithm>


// Принимает только объекты-диапазоны
template <rng::input_range Range>
void Print(const Range& range) {}
// OR
void Print(const rng::input_range auto& range)


// В качестве компаратора взят обычный std::less, который сравнивает, применяя операцию <, но благодаря проекции применяется он не к элементам вектора, а к значениям лямбда-функции.
rng::sort(lectures, std::less<>{}, [](const Lecture& x) {
    return x.complexity;
});



// взять с 5 по 10 элемент последовательности
for (const auto& x : tab | view::drop(5) | view::take(10)) {
    std::cout << x << std::endl;
}
// Адаптеров в стандартной библиотеке не так много, как алгоритмов, а жаль. Вот список всех: all, filter, transform, take, take_while, drop, drop_while, join, split, counted, common, reverse, elements, keys, values.

```

![range_alg](images/range_alg.jpeg)


<a name="threads"></a>

# Многопоточность(threads)

- join: ожидание завершения исполнения потока
- detach: продолжение исполнения родительского потока без ожидания завершения
- get_id: возвращает идентификатор потока (не обязан совпадать с системным)
- joinable: возвращает флаг "активности"потока
- native_handle: возвращает "нативный"хэндл потока
- hardware_concurrency: статический метод, возвращает количество потоков, которые могут выполняться параллельно.


Функции this_tread:
- get_id: возвращает идентификатор потока исполнения, в котором она вызвана
- yield: приостановка выполнения
- sleep_until: приостановка выполнения до наступления момента, переданного в качестве аргумента
- sleep_for: приостановка выполнения на заданный промежуток времени

```c++
#include <thread>

auto func = [](int num1, int num2){
    std::cout << num1 << num2 << std::endl;
};
std::thread thread(func, 1, 2); // создание потока
std::thread thread(func, std::ref(num_1), num_2); // передача аргумента по ссылке

std::cout << std::hex << std::this_thread::get_id() << std::endl; // id потока

// приостановка потока на 5 минут
auto time = std::chrono::system_clock::now() + std::chrono::minutes(5);
std::this_thread::sleep_until(time);
std::this_thread::sleep_for(std::chrono::minutes(5));
``` 
### async and future

```c++
#include <future>

int SumToVector(const vector<int>& one, const vector<int>& two){
    future <int> f = async ([&one] {
        return accumulate ( begin (one), end(one), 0);
    });
    return  f.get() +  accumulate ( begin (two), end(two), 0);
}
```

- future<T>, созданные с помощью async, вызывает get() в своём деструктуре
- если результат вызова функции async не сохранить в переменную, то программа может выполняться последовательно

```c++
auto husband= async ( SpendMoney , ref(family_account)); // передача по ссылке
```

### mutex and lock_guard

В программировании это тот участок кода, который в любой момент времени может выполнять не более одного потока.

```c++
#include <mutex>

mutex m;
lock_guard<mutex> g(m);
// Он захватывает мьютекс в конструкторе и освобождает в деструкторе
```


В стандарте С++17 были введены параллельные версии стандартных алгоритмов. Чтобы получить параллельную версию алгоритма for_each достаточно подключить заголовочный файл execution и в качестве первого параметра в функции указать execution::par.
```c++
for_each ( execution ::par , ...)
```


<a name="constexpr"></a>

# Константные вычисления

C++11:
- constexpr
C++20:
- consteval
- constinit

constexpr означает, что если значения параметров возможно посчитать на этапе компиляции, то и возвращаемое значение должно быть посчитано на этапе компиляции. Если значение хотя бы одного параметра будет неизвестно на этапе компиляции, то функция будет запущена как обычная, в runtime.

Syntax:

```c++
constexpr тип_возврата имя_функции(параметры)

Example:
constexpr int Sum(int a, int b) {
return a + b;
}

constexpr тип имя = выражение;
constexpr в этом случае означает создание константы, выражение должно быть известно на этапе компиляции.

constexpr int b = Sum(10, 20); // OK
constexpr int c = 10 * a; // error C2131
```

В C++17 появилась конструкция if constexpr

```c++
template <typename T>
std::string str(T t) {
    if constexpr (std::is_same_v<T, std::string>)
        return t;
    else
        return std::to_string(t);
}
```

consteval - аналог constexpr, но ВСЕГДА выполняется во время компиляции (при невозможности произойдет ошибка компиляции).

Спецификатор constinit может быть применен к переменным, имеющим статическую продолжительность жизни или продолжительность жизни потока. constinit гарантирует, что переменные будут инициализированы во время компиляции.

<a name="corutine"></a>

# Корутины

Корутина (coroutine, сопрограмма) - функция, которая может
приостанавливать свое выполнение.

Вместе с корутинами C++ обзавелся несколькими новыми операторами:
- co_await: приостановка выполнения корутины в ожидании результата
- co_yield: возврат значения с приостановкой выполнения корутины
- co_return: возврат значения с уничтожением корутины

```c++

```


<a name="model_storage"></a>

# Модель памяти


Когда в программе запускается очередная функция, то на стеке резервируется блок памяти, достаточный для хранения локальных переменных. Кроме того, в этом блоке хранится служебная информация, такая как адрес возврата. Такая область памяти называется стековым фреймом функции. Стековый фрейм перетирает данные, которые были в стеке от предыдущих вызовов.

![stack_frame](images/stack_frame.png)

Для хранения элементов вектора нам подходит источник памяти, который называется «куча». Во время работы программа может в любой момент обращаться в кучу и выделять в ней блок памяти произвольного размера или освобождать ранее выделенный.

![heap_frame](images/heap_frame.png)

В модели памяти С++ есть два источника памяти: стек и куча. В стеке размещаются локальные переменные функций. В куче размещаются объекты, которые должны жить дольше, чем создавшая их функция.

Стек вызовов:
Linux -> 10 Mb
Windows -> 1 Mb

<a name="new_delete"></a>

# New && delete

![new](images/new.png)

```c++
int∗ pInt = new int;


string ∗ s = new string ;
∗s = " Hello";
cout << ∗s << ' ' << s−>size () << endl;
```

new – это специальный оператор, который выполняет выделение памяти из кучи. Оператор int* подсказывает, для хранения какого типа нам нужна память. При этом pInt – это указатель на значение типа int. Сама переменная pInt будет храниться в стеке, но она будет указывать на область памяти в куче.


Указатель – это адрес в памяти. Память в С++ представляется как линейный массив байтов. 
Указатель – это индекс в этом массиве.

Оператор *, примененный к указателю, возвращает ссылку на объект в куче.
При вызове оператора new для объектов классового типа оператор new не только выделяет необходимую память в куче, но и вызывает конструктор по умолчанию.

Если нам в куче нужно выделить size объектов. Пока мы умеем создавать только один объект. Несколько объектов можно создать с помощью оператора new[]. Он создает блок памяти для хранения необходимого количества объектов.

```c++
explicit SimpleVector ( size_t size){
	data = T [size ];
}
```

Сейчас в нашей программе есть утечка памяти. Освобождать её следует в деструкторе.

```c++
~ SimpleVector () {
	delete [] data;
}
```
Если вмето delete[] пропишем просто delete, то программа будет работать некорректно.
 

Константный указатель и указатель на константу:

![const_ptr](images/const_ptr.png)

<a name="gtest"></a>

# Google test

```bash
git clone https://github.com/google/googletest.git
```

<a name="move"></a>

# Move-семантика

Перемещение помогает сэкономить на копировании данных из кучи

Разрешено, если источник является временным объектом и таким образом отказывается от своих данных в куче

Временные объекты бывают двух видов:
- результатом вызова функции
- результат вызова конструктора


Когда может возникнкть перемещение:
- Инициализация
- Присваивание

Move:

```c++
strings.push_back(move(s));
```

- Функция move позволяет инициировать перемещение там, где случилось бы копирование 
- Сама функция move не перемещает, она лишь заставляет компилятор переместить
- Как правило, после перемещения объект остается пустым

Когда перемещение не поможет:
- Если у объекта нет данных в куче, а основные данные на стеке, то данные придётся копировать.
- Вызов move для константного объекта бесполезен

Конструктор копирования и оператор присваивания:

```c++
// Ситуации где происходит копирование
vector<int> source = ...
vector<int> target = source
// or
target = source;
target.operator=(source)

// Перегрузка
MyClass(const MyClass&) = default;
MyClass operator=(const MyClass&) = default; // принимает l-value ссылку
// Для классов, где ведется управление с памятью надо самому реалзовыть перегрузку
```

Конструктор перемещения и перемещающий оператор присваивания:

```c++
// Ситуации где происходит перемещение
vector<int> source = ...
vector<int> target = move(source)
vector<int> target(move(source))
// or
target.push_back(vector<int>(5));
target = move(source);
target = vector<int>(5);


// Перегрузка
MyClass(MyClass&&) = default;
MyClass operator=(MyClass&&) = default; // принимает r-value ссылку
// Если конструктор копирования перегружен, то при отсутствии оператора перемещения, выполняется копирование
```
Работа функций и с времеными объектами и с обычными
```c++
void ChangeFirstName (int year , string first_name ) {
    first_names [year] = move( first_name );
}
// tempory: move -> move
// else: copy -> move
```

Move-итератор

```c++
#include <iterator>

sentences.push_back(Sentence <Token >(
    make_move_iterator(tokens_begin),
    make_move_iterator(sentence_end)
));

// Вместо copy можно использовать move
move(begin(permutation), end(permutation), range_begin);
```


### [INFO!] В языке есть такие типы, которые копировать бессмысленно, например, потоки ввода и вывода. Разрешено только перемещение. Копирование порождает ошибку "use of deleted function ..."


NRVO и copy elision

Компилятор может опускать и перемещение и копирование

```c++
Logger MakeLogger () {
    return Logger (); // temporary -> returned temporary
}
```

- при возвращении из функции временного объекта;
- при инициализации нового объекта временным объектом.

Такая оптимизация называется copy elision.

При возвращении локальной переменной из функции перемещение и копирование опускаются.

Такая оптимизация называется named return value optimization.

```c++
Logger MakeLogger () {
    Logger log; 
    ...
    return log; // return local varible
}
```

Опасности return

Example 1:

```c++
pair <ifstream , ofstream > MakeStreams ( const string & prefix ) {
    ifstream input ( prefix + ".in");
    ofstream output ( prefix + ".out");
    return {input , output };
}

/*
Код не компилируется, компилятор не может составить пару потоков. input и output передаются в конструктор пары, затем созданная с помощью этого конструктора пара должна проинициализировать возвращаемый временный объект. Поскольку эта пара тоже временная, то инициализация временного объекта, возвращаемого из функции этой парой, происходит безбо-
лезненно, но передача переменных input и output в конструктор пары происходит по обычным правилам языка С++. Чтобы решить проблему, следует обернуть input и output в move.
*/

pair <ifstream , ofstream > MakeStreams ( const string & prefix ) {
    ifstream input ( prefix + ".in");
    ofstream output ( prefix + ".out");
    return {move(input) , move(output)};
}


pair <ifstream , ofstream > MakeStreams ( const string & prefix ) {
    return {ifstream(prefix + ".in"), ofstream(prefix + ".out")};
}
```

Example 2:

```c++
ifstream MakeInputStream ( const string & prefix ) {
    auto streams = MakeStreams ( prefix );
    return streams . first ;
}

/*
streams.first не является временным объектом. Также это выражение не является названием локальной переменной – это поле локальной переменной
*/

ifstream MakeInputStream ( const string & prefix ) {
    auto streams = MakeStreams ( prefix );
    return move(streams.first);
}


ifstream MakeInputStream ( const string & prefix ) {
    auto streams = MakeStreams ( prefix );
    return move(streams).first;
}
```


<a name="user_literal"></a>

# Пользовательские литералы


```c++
using ull = unsigned long long;

constexpr ull operator"" _KB(ull no)
{
    return no * 1024;
}

constexpr ull operator"" _MB(ull no)
{
    return no * (1024_KB);
}

cout<<1_KB<<endl;
cout<<5_MB<<endl;
```

<a name="func_str"></a>

# Функции для работы со строками

```c++
// Совпадение двух строк(0 - если совпадают)
#include <string.h>

strcmp(str_1, str_2);

```
