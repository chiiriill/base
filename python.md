# Python 

## Content:

- <a href="#pydantic">Pydantic</a>
- <a href="#hashlib">Hashlib</a>


<a name="pydantic"></a>

## Pydantic

```sh
pip install pydantic 
```


```py
from pydantic import BaseModel, Field

class Trade(BaseModel):
    id: int
    user_id: int
    currency: str = Field(max_length=5)
    side: str
    price: float = Field(ge=0)
    amount: float

```

<a name="hashlib"></a>

## Hashlib

Модуль hashlib реализует общий интерфейс для множества различных безопасных алгоритмов хеширования и дайджеста сообщений. В модуль включены алгоритмы безопасного хеширования, такие как FIPS SHA1, SHA224, SHA256, SHA384 и SHA512, определенные в FIPS 180-2, а также алгоритм MDA RSA, определенный в Интернете RFC 1321.

- Если нужны хеш-функции adler32 или crc32, то они доступны в модуле zlib.

Все функции возвращают хеш-объект с одинаковым простым интерфейсом. Этот объект можно "кормить" байтоподобными объектами, используя метод `HASH.update()`. В любой момент можно запросить дайджест(хеш) о конкатенации данных, переданных в него, используя методы `HASH.digest()` или `HASH.hexdigest()`.

```py
import hashlib

# вычисление хэш
hashlib.sha224(b"Nobody inspects the spammish repetition").hexdigest()

# вычисление хеш-суммы файла
with open('/usr/bin/python3', 'rb') as f:
    hsh = hashlib.sha1()
    while True:
        data = f.read(2048)
        if not data:
            break
        hsh.update(data)
    rez = hsh.hexdigest()

print(rez)
```


## Построения классов, содержащих только атрибуты и никаких методов

```python
import collections
Card = collections.namedtuple('Card', ['rank', 'suit'])
```

## Строковое представление

#### Мы используем !r для получения стандартного представления отображаемых атрибутов. Это разумный подход, потому что в нем отчетливо проявляется существенное различие между Vector(1, 2) и Vector('1', '2')


```python
def __repr__(self):
return f'Vector({self.x!r}, {self.y!r})'
```

## Булево значение пользовательского типа

#### По умолчанию любой экземпляр пользовательского класса считается истинным, но положение меняется, если реализован хотя бы один из методов __bool__ или __len__. Функция bool(x), по существу, вызывает x.__bool__() и использует полученный результат. Если метод __bool__ не реализован, то Python пытается вызвать x.__len__() и при получении нуля функция bool возвращает False. В противном случае bool возвращает True.

## Dunder methods(магические метод)

![dunder_1](images/dunder_1.png)
![dunder_2](images/dunder_2.png)
![dunder_3](images/dunder_3.png)

## Встроенные контейнеры 

### Контейнерные последовательности
Позволяют хранить элементы разных типов, в т. ч. вложенные контейнеры.
Примерами могут служить list, tuple и collections.deque.

### Плоские последовательности
Позволяют хранить элементы только одного типа. Примерами могут слу-
жить str, bytes и array.array.

## Подключение стандартного потока ввода 

```py
from sys import stdin

# 1

lines = []
for line in stdin:
    lines.append(line)

# 2

lines = stdin.readlines()

# One line
text = stdin.read() 

print(lines)

```

### Чтобы остановить ввод надо использовать сочетание клавищ Ctrl+D

## print() в файл

```py
with open("output_3.txt", "w", encoding="UTF-8") as file_out:
    print("Вывод в файл с помощью функции print()", file=file_out)
```

## JSON

### Для чтения JSON-файлов используется метод load(), считывающий весь JSON-файл целиком и возвращающий объект стандартного типа данных Python.

### ля записи изменённых данных в JSON-файл используется метод dump(). Рассмотрим некоторые важные его аргументы:
-    ensure_ascii. Имеет значение по умолчанию True, при котором все не-ASCII-символы при выводе в файл представляют собой юникод-последовательности вида \uXXXX (коды символов в таблице кодировки). Если аргумент имеет значение False, такие символы будут записаны в виде символов, а не их кодов. В примере используются русские символы, поэтому необходимо передать в аргумент значение False.
-    indent. Задаёт вид отступа для удобства чтения данных человеком. По умолчанию аргумент имеет значение None, а данные записываются в файл одной строкой. Если задать строку вместо None, то эта строка будет использоваться в качестве отступа. Если задать число больше 0, то отступ будет состоять из такого же количества пробелов.
-    sort_keys. Позволяет отсортировать ключи словаря с данными. По умолчанию имеет значение False. Для сортировки ключей необходимо передать в аргумент значение True.


## Библиотека math

```py
import math

print(math.comb(12, 3)) #  сколькими способами можно выбрать 3 объекта из множества в 12 объектов (порядок не важен)

print(math.factorial(5)) # факториал

print(math.gcd(120, 210, 360))  # возвращает наибольший общий делитель (НОД)

print(math.lcm(10, 20, 30, 40))  #  возвращает наименьшее общее кратное (НОК)

# возвращает количество размещений из n элементов по k элементам без повторений и с учётом порядка. Если значение аргумента k не задано, то возвращается количество перестановок множества из n элементов:
print(math.perm(4, 2)) 
print(math.perm(4))  

print(math.prod(range(10, 21))) # возвращает произведение элементов итерируемого объекта iterable

print(math.dist((0, 0, 0), (1, 1, 1)))  # возвращает Евклидово расстояние между точками p и q
```

## Переменные окружения(env)

Хранятся в .env для заливки .env.example


