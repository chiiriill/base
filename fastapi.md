# FastAPI

## Content:

- <a href="#install">Установка</a>
- <a href="#query">Query-параметры</a>
- <a href="#path">Path-параметры</a>
- <a href="#body">Body</a>
- <a href="#response">Ответ(Response)</a>
- <a href="#form">Данные формы(Form)</a>
- <a href="#cookie">Cookie</a>
- <a href="#headers">Headers</a>
- <a href="#file">Раота с файлами</a>
- <a href="#error">Обработка ошибок(error)</a>
- <a href="#dependency">Зависимости(dependency)</a>
- <a href="#middleware">Middleware</a>
- <a href="#cors">CORS(Cross-Origin Resource Sahring)</a>
- <a href="#docs">Просмотр документации</a>
- <a href="#request">Обработка запросов</a>
- <a href="#validation">Валидация</a>
- <a href="#type">Типы данных</a>
- <a href="#background">Фоновые задачи</a>
- <a href="#authentication">Аутентификация</a>
- <a href="#authorization">Авторизация</a>
- <a href="#db">Базы данных</a>


<a name="install"></a>

## Установка

```bash
pip install fastapi
pip install uvicorn
```

<a name="query"></a>

## Query-параметры

```py
# ... - значит что это обязательный параметр, или Required из pydantic 
# list[str] - может несколько раз передаваться
@app.get("/items/")
async def read_items(q: Annotated[list[str] | None, Query(max_length=50, min_length=20, pattern="^hi$")] = ...):
    results = {"items": [{"item_id": "Foo"}, {"item_id": "Bar"}]}
    if q:
        results.update({"q": q})
    return results
```
Метаданные в Query():
-    alias - псевдоним для поля
-    title - название
-    description - описание
-    deprecated - объявить как устаревший
-    include_in_schema - исключить из OpenAPI
-    gt: больше (greater than)
-    ge: больше или равно (greater than or equal)
-    lt: меньше (less than)
-    le: меньше или равно (less than or equal)


<a name="path"></a>

## Path-параметры

```py
@app.get("/items/{item_id}")
async def read_items(
        item_id: Annotated[int, Path(title="The ID of the item to get")],
        q: Annotated[str | None, Query(alias="item-query", default=None)],
):
    results = {"item_id": item_id}
    if q:
        results.update({"q": q})
    return results

```

### Метаданные справедливы такие же, как и для Query, Body

<a name="body"></a>

## Body

```py
# Body(embed=True) - Передача параметров как JSON с ключом item 

@app.put("/items/{item_id}")
async def update_item(item_id: int, item: Annotated[Item, Body(embed=True)]):
    results = {"item_id": item_id, "item": item}
    return results

```


### Метаданные справедливы такие же, как и для Query, Path


<a name="response"></a>

## Ответ(Response)

```py
# response_model_exclude_unset=True - начения по умолчанию не будут включены в ответ. В нем будут только те поля, значения которых фактически были установлены.
# response_model_include={"name", "description"} - какие поля включить в вывод
# response_model_exclude={"tax"} - какие поля исключить из вывода
@app.post("/user/", response_model=UserOut, response_model_exclude_unset=True)
async def create_user(user: UserIn) -> Any:
    return user

####

from fastapi import FastAPI, Response
from fastapi.responses import JSONResponse, RedirectResponse

@app.get("/portal")
async def get_portal(teleport: bool = False) -> Response:
    if teleport:
        return RedirectResponse(url="https://www.youtube.com/watch?v=dQw4w9WgXcQ")
    return JSONResponse(content={"message": "Here's your interdimensional portal."})


# Status response

@app.post("/items/", status_code=201)
async def create_item(name: str):
    return {"name": name}

# OR
from fastapi import status

@app.post("/items/", status_code=status.HTTP_201_CREATED)
async def create_item(name: str):
    return {"name": name}


```

<a name="form"></a>

## Данные формы(Form) 

```py
from fastapi import FastAPI, Form


@app.post("/login/")
async def login(username: Annotated[str, Form()], password: Annotated[str, Form()]):
    return {"username": username}

```


<a name="cookie"></a>

## Cookie

FastAPI поддерживает три типа файлов cookie:

- Обычные файлы cookie: Это стандартные файлы cookie, которые хранятся на стороне клиента и отправляются обратно на сервер при каждом запросе. Обычные файлы cookie широко используются для различных целей, таких как управление сеансами и пользовательскими настройками.

- Защищенные файлы cookie: Защищенные файлы cookie - это зашифрованные файлы cookie, которые обеспечивают дополнительный уровень безопасности. Под защищенными куками мы имеем ввиду куки, в которых установлен флаг Secure, который говорит браузеру, что эти куки можно передавать только по SSL-соединению.

```
    Примечание: рекомендуем прочитать про XSS-атаки, и дополнительную защиту куков при помощи установки флага HttpOnly, чтобы гарантировать, что данные файлов cookie остаются конфиденциальными и не могут быть использованы злоумышленниками с использованием внедрения вредоносного Javascript'а на сайтах.
```

- Подписанные файлы cookie: Подписанные файлы cookie - это файлы cookie, которые содержат подпись для проверки их подлинности. Они гарантируют, что данные cookie-файлов не были изменены клиентом с тех пор, как они были установлены сервером.

```py
# Взятие cookie
@app.get("/")
def root(last_visit = Cookie()):
    return  {"last visit": last_visit}


from fastapi import Response
# Установка cookie
@app.get("/set_cookies")
def set_cookies(response: Response):
    now = datetime.now()
    response.set_cookie(key="lastvisit", value=now)
    # httponly=True - httponly - недоступны для вредоносного JS; флаг secure означает, что куки идут только по HTTPS
    return {"message": "cookie set"}
```


Вы можете указать время истечения срока действия файла cookie, используя параметр "expires", или установить максимальный возраст, используя параметр "max_age". Это помогает контролировать срок службы файлов cookie и эффективно управлять данными сеанса.
FastAPI предоставляет простой способ удалить файлы cookie, установив время их истечения в прошлом. Это дает указание клиенту удалить файл cookie из своего хранилища.

Также у класса Response есть метод delete_cookie, который принимает в качестве аргумента строку (наименование куки) и удаляет ее на стороне клиента.

Для примера:

```py
@router.post("/logout", status_code=204)
async def logout_user(response: Response):
    response.delete_cookie("example_access_token")
```
Под капотом, этот метод вызывает метод set_cookie и устанавливает атрибутам max_age и expires значение 0.

<a name="headers"></a>

## Headers

HTTP-заголовки - это метаданные, которые сопровождают HTTP-запрос или ответ. FastAPI позволяет вам получать доступ к заголовкам запросов и работать с ними для извлечения важной информации, такой как токены аутентификации, юзер-агенты и типы контента.

```py
# получение 
@app.get("/")
def root(user_agent: str = Header()):
    return {"User-Agent": user_agent}

# OR

@app.get("/headers")
async def get_head(request: Request):
    try:
        return {
            "user_agent": request.headers["user-agent"],
            "accept_language": request.headers["accept-language"]
        }
    except KeyError:
        return HTTPException(status_code=401, detail="Error check")



# отправка 
@app.get("/")
def root():
    data = "Hello from here"
    return Response(content=data, media_type="text/plain", headers={"Secret-Code": "123459"})

# OR

@app.get("/")
def root(response: Response):
    response.headers["Secret-Code"] = "123459"
    return {"message": "Hello from my api"}
```


<a name="file"></a>

## Работа с файлами


```py
# Скачивание файла при переходе по api
@app.get("/file/download")
def download_file():
    return FileResponse(path='../index.html', filename='SecretPage.html', media_type='multipart/form-data')


# Получение строки байтов
@app.post("/file/upload-bytes")
def upload_file_bytes(file_bytes: bytes = File()):
    return {'file_bytes': str(file_bytes)}


# Получение файла
@app.post("/file/upload-file")
def upload_file(file: UploadFile):
    contents = await file.read() # get content file
    return file.filename

# Example HTML form to load files
@app.get("/")
async def main():
    content = """
<body>
<form action="/files/" enctype="multipart/form-data" method="post">
<input name="files" type="file" multiple>
<input type="submit">
</form>
<form action="/uploadfiles/" enctype="multipart/form-data" method="post">
<input name="files" type="file" multiple>
<input type="submit">
</form>
</body>
    """
    return HTMLResponse(content=content)
```

<a name="error"></a>

## Обработка ошибок(error)

```py
from fastapi import FastAPI, Request
from fastapi.responses import JSONResponse


class UnicornException(Exception):
    def __init__(self, name: str):
        self.name = name


app = FastAPI()


@app.exception_handler(UnicornException)
async def unicorn_exception_handler(request: Request, exc: UnicornException):
    return JSONResponse(
        status_code=418,
        content={"message": f"Oops! {exc.name} did something. There goes a rainbow..."},
    )


@app.get("/unicorns/{name}")
async def read_unicorn(name: str):
    if name == "yolo":
        raise UnicornException(name=name)
    return {"unicorn_name": name}

```

Коды ответа:

-    1XX – статус-коды информационного типа. Они редко используются разработчиками напрямую. Ответы с этими кодами не могут иметь тела.
-    2XX – статус-коды, сообщающие об успешной обработке запроса. Они используются чаще всего.
-    3XX – статус-коды, сообщающие о перенаправлениях. Ответы с этими кодами статуса могут иметь или не иметь тело, за исключением ответов со статусом 304, "Not Modified", у которых не должно быть тела.
-    4XX – статус-коды, сообщающие о клиентской ошибке. Это ещё одна наиболее часто используемая категория.
-    5XX – статус-коды, сообщающие о серверной ошибке. Они почти никогда не используются разработчиками напрямую. Когда что-то идет не так в какой-то части кода вашего приложения или на сервере, он автоматически вернёт один из 5XX кодов.


<a name="dependency"></a>

## Зависимости(dependency)

```py

async def common_parameters(q: str | None = None, skip: int = 0, limit: int = 100):
    return {"q": q, "skip": skip, "limit": limit}


CommonsDep = Annotated[dict, Depends(common_parameters)]


@app.get("/items/")
async def read_items(commons: CommonsDep):
    return commons


class CommonQueryParams:
    def __init__(self, q: str | None = None, skip: int = 0, limit: int = 100):
        self.q = q
        self.skip = skip
        self.limit = limit

# use_cashe=False - отключает при надобности кэширование
@app.get("/items/")
async def read_items(commons: Annotated[CommonQueryParams, Depends(CommonQueryParams, use_cashe=Fasle)]):
    response = {}
    if commons.q:
        response.update({"q": commons.q})
    items = fake_items_db[commons.skip: commons.skip + commons.limit]
    response.update({"items": items})
    return response

# dependency in path

async def verify_key(x_key: Annotated[str, Header()]):
    if x_key != "fake-super-secret-key":
        raise HTTPException(status_code=400, detail="X-Key header invalid")
    return x_key


@app.get("/items/", dependencies=[Depends(verify_token), Depends(verify_key)])
async def read_items():
    return [{"item": "Foo"}, {"item": "Bar"}]

# dependency to all project

app = FastAPI(dependencies=[Depends(verify_token), Depends(verify_key)])

```

<a name="middleware"></a>

## Middleware

`Middleware` — это функция, которая работает с каждым запросом до того, как он будет обработан какой-либо конкретной операцией пути . А также с каждым ответом перед его возвратом.

```py
@app.middleware('http')
async def add_process_time_header(request: Request, call_next):
    start_time = time.time()
    response = await call_next(request)
    process_time = time.time() - start_time
    response.headers["X-Process-Time"] = str(process_time)
    return response
```

<a name="cors"></a>

## CORS(Cross-Origin Resource Sahring)

```py
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

origins = [
    "http://localhost.tiangolo.com",
    "https://localhost.tiangolo.com",
    "http://localhost",
    "http://localhost:8080",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
```


-    allow_origins - Список источников, на которые разрешено выполнять кросс-доменные запросы. Например, ['https://example.org', 'https://www.example.org']. Можно использовать ['*'], чтобы разрешить любые источники.
-    allow_origin_regex - Регулярное выражение для определения источников, на которые разрешено выполнять кросс-доменные запросы. Например, 'https://.*\.example\.org'.
-    allow_methods - Список HTTP-методов, которые разрешены для кросс-доменных запросов. По умолчанию равно ['GET']. Можно использовать ['*'], чтобы разрешить все стандартные методы.
-    allow_headers - Список HTTP-заголовков, которые должны поддерживаться при кросс-доменных запросах. По умолчанию равно []. Можно использовать ['*'], чтобы разрешить все заголовки. Заголовки Accept, Accept-Language, Content-Language и Content-Type всегда разрешены для простых CORS-запросов.
-    allow_credentials - указывает, что куки разрешены в кросс-доменных запросах. По умолчанию равно False. Также, allow_origins нельзя присвоить ['*'], если разрешено использование учётных данных. В таком случае должен быть указан список источников.
-    expose_headers - Указывает любые заголовки ответа, которые должны быть доступны браузеру. По умолчанию равно [].
-    max_age - Устанавливает максимальное время в секундах, в течение которого браузер кэширует CORS-ответы. По умолчанию равно 600.


<a name="docs"></a>

## Просмотр документации

```http
http://localhost:8000/docs
http://127.0.0.1:8000/redoc
```

```py
@app.post(
    "/items/",
    response_model=Item,
    summary="Create an item", # краткое описание
    response_description="The created item", # описание ответа
)
async def create_item(item: Item):
    """
    Create an item with all the information:

    - **name**: each item must have a name
    - **description**: a long description
    - **price**: required
    - **tax**: if the item doesn't have tax, you can omit this
    - **tags**: a set of unique tag strings for this item
    """
    return item


@app.get("/elements/", 
        tags=["items"], # привязка тега к запросу 
        deprecated=True # указать как устаревший
        )
async def read_elements():
    return [{"item_id": "Foo"}]

```

<a name="request"></a>

## Обработка запросов

```py
@app.get()
@app.post()
@app.put()
@app.delete()
@app.options()
@app.head()
@app.patch()
@app.trace()


@app.patch("/items/{item_id}", response_model=Item)
async def update_item(item_id: str, item: Item):
    stored_item_data = items[item_id]
    stored_item_model = Item(**stored_item_data)
    update_data = item.dict(exclude_unset=True)
    updated_item = stored_item_model.copy(update=update_data)
    items[item_id] = jsonable_encoder(updated_item)
    return updated_item

```

<a name="validation"></a>

## Валидация

```py

class UserCreate(BaseModel):
    name: str
    email: str
    age: int
    is_subscribed: Union[bool, None] = None


    @validator("age")
    @classmethod
    def validate_age(cls, value):
        if value < 0:
            raise ValueError("User must be positive")
        return value


    @validator("email")
    @classmethod
    def validate_email(cls, value):
        if not bool(re.fullmatch(r'[\w.-]+@[\w-]+\.[\w.]+', value)):
            raise ValueError("Email is invalid")
        return value

    # Свой пример для документации
    model_config = {
        "json_schema_extra": {
            "examples": [
                {
                    "name": "Foo",
                    "description": "A very nice Item",
                    "price": 35.4,
                    "tax": 3.2,
                }
            ]
        }
    }

    # OR

    name: str = Field(example="goo")
    description: str | None = Field(default=None, examples=["A very nice Item"])
    price: float = Field(examples=[35.4])
    tax: float | None = Field(default=None, examples=[3.2])

@app.get("/users/{user_id}", response_model=list[User])
async def get_user(user_id: int):
    return [user for user in fake_users if user.get("id") == user_id]


class Trade(BaseModel):
    id: int
    user_id: int
    currency: str = Field(max_length=5)
    side: str
    price: float = Field(ge=0)
    amount: float
```

<a name="type"></a>

## Типы данных

- int
- float
- str
- bool
- другие стандартные типы Python'а

Но вы также можете использовать и более сложные типы. 

- UUID 
- datetime.time
- frozenset
- Decimal
- bytes
- Annotated из пакета typing 

### Преобразовать Pydantic в Json правильный

```py
from fastapi.encoders import jsonable_encoder

@app.put("/items/{id}")
def update_item(id: str, item: Item):
    json_compatible_item_data = jsonable_encoder(item)
    fake_db[id] = json_compatible_item_data
```


<a name="background"></a>

## Фоновые задачи

Фоновые задачи полезны для обработки задач, которые не должны блокировать основной цикл запроса-ответа, таких как отправка электронных писем, обновление аналитики или выполнение дополнительной неблокирующей обработки.

```py

app = FastAPI()


def write_notifications(email: str, message: str = ""):
    with open("log", mode="a", encoding="utf-8") as email_file:
        content = f"notification for {email}: {message}\n"
        email_file.write(content)


@app.get("/send-notification/{email}")
async def send_notification(email: str, background_tasks: BackgroundTasks):
    background_tasks.add_task(write_notifications, email, message="some notification")
    return {"message": "Notification sent in the log file"}

```

<a name="authentication"></a>

## Аутентификация

Аутентификация - это процесс проверки личности пользователя или системы, пытающихся получить доступ к веб-приложению. Это гарантирует, что только авторизованные пользователи смогут получить доступ к защищенным ресурсам или выполнить определенные действия. 

Базовая аутентификация - это простой метод, при котором клиент (обычно веб-браузер или клиент API) вводит имя пользователя и пароль в заголовок запроса `Authorization`. Затем сервер проверяет эти учетные данные в базе данных пользователя или у поставщика аутентификации. FastAPI обеспечивает встроенную поддержку базовой аутентификации, что упрощает ее реализацию.

```py
from fastapi import FastAPI, Depends, status, HTTPException
from pydantic import BaseModel
from fastapi.security import HTTPBasic, HTTPBasicCredentials

app = FastAPI()
security = HTTPBasic()


class User(BaseModel):
    username: str
    password: str


# добавим симуляцию базы данных в виде массива объектов юзеров
USER_DATA = [User(**{"username": "user1", "password": "pass1"}), User(**{"username": "user2", "password": "pass2"})]


# симуляционный пример
def get_user_from_db(username: str) -> User | None:
    for user in USER_DATA:
        if user.username == username:
            return user
    return None


def authenticate_user(credentials: HTTPBasicCredentials = Depends(security)) -> User:
    user = get_user_from_db(credentials.username)
    if user is None or user.password != credentials.password:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid credentials")
    return user


@app.get("/protected_resource/")
def get_protected_resource(user: User = Depends(authenticate_user)):
    return {"message": "You have access to the protected resource!", "user_info": user}
```

### На основе JWT

JWT расшифровывается как веб-токен JSON, и это популярный метод реализации аутентификации в веб-приложениях. В отличие от базовой аутентификации, где серверу необходимо поддерживать состояние (обычно с помощью сессионных файлов cookie или токенов), JWT допускает аутентификацию без сохранения состояния. 

JWT - это объект JSON с цифровой подписью, который содержит утверждения о пользователе.

Утверждения - это утверждения о сущности (обычно о пользователе) и дополнительных данных.

JWT обычно состоит из трех частей, разделенных точками: заголовка, полезной нагрузки и подписи. Заголовок содержит метаданные о токене, такие как алгоритм подписи. Полезная нагрузка содержит утверждения, которые могут включать пользовательскую информацию и другие пользовательские данные. Подпись генерируется с использованием секретного ключа, известного только серверу.

Рабочий процесс аутентификации на основе JWT выглядит следующим образом:

- Шаг 1: Аутентификация пользователя. Когда пользователь предоставляет действительные учетные данные (например, имя пользователя и пароль) для входа в систему, сервер проверяет их и генерирует JWT.
- Шаг 2: Отправка JWT клиенту. Сервер отправляет JWT обратно клиенту (обычно в заголовке `Authorization` ответа) в качестве токена.
- Шаг 3: Последующие запросы. Для всех последующих запросов, требующих аутентификации, клиент отправляет JWT в заголовке `Authorization`. Затем сервер проверяет подпись JWT с помощью секретного ключа. Если подпись действительна, сервер извлекает информацию о пользователе из утверждений и разрешает доступ к запрошенным ресурсам.

Хотя аутентификация на основе JWT дает множество преимуществ, она также сопряжена с соображениями безопасности:
- Срок действия токена: установите подходящее время истечения срока действия токена, чтобы ограничить его срок действия.
- Используйте HTTPS: Всегда используйте HTTPS для шифрования связи между клиентом и сервером.
- Защита секретного ключа: Сохраняйте секретный ключ, используемый для подписи JWTS, в безопасности. Компрометация ключа может привести к нарушениям безопасности.
- Размер токена: Помните о размере токена, поскольку он отправляется с каждым запросом. Избегайте включения конфиденциальных или ненужных данных в полезную нагрузку.

<a name="authorization"></a>

## Авторизация

```py



```

<a name="db"></a>

## Базы данных

FastAPI поддерживает широкий спектр баз данных, включая:

-    SQLite: Облегченная бессерверная база данных, которая хранит данные в локальном файле.
-    PostgreSQL: Мощная система управления реляционными базами данных с открытым исходным кодом, известная своей производительностью и масштабируемостью. 
-    MySQL: Популярная система реляционных баз данных, которая известна своей надёжностью.
-    MongoDB: База данных NoSQL (нереляционная), которая хранит данные в гибких документах, подобных JSON.

Чтобы подключить FastAPI к базе данных, вам необходимо выполнить следующие общие действия:

-    Шаг 1: Установите драйвер базы данных. Сначала установите соответствующий драйвер базы данных для выбранной вами базы данных. Например, если вы планируете использовать PostgreSQL, установите библиотеку `asyncpg` для асинхронного доступа или `psycopg2` для синхронного доступа. 
-    Шаг 2: Импортируйте драйвер базы данных. В вашем приложении FastAPI импортируйте необходимый модуль драйвера базы данных.
-    Шаг 3: Настройте подключение к базе данных. Настройте параметры подключения к базе данных, такие как URL-адрес базы данных, имя пользователя, пароль и другие соответствующие конфигурации.
-    Шаг 4: Создайте сеанс работы с базой данных. Создайте сеанс базы данных или пул подключений, который ваше приложение FastAPI может использовать для взаимодействия с базой данных.
